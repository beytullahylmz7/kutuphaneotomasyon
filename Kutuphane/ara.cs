﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Sql;
using System.Data.SqlClient;

namespace Kutuphane
{
    public partial class ara : Form
    {
        public ara()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection(@"Data source=.\;Database=.;Integrated Security=true;");
        // SqlConnection conn = new SqlConnection(@"Data source=.\SQLEXPRESS;Database=.;Integrated Security=true;");

        private void button4_Click(object sender, EventArgs e)
        {
            Deposit sa = new Deposit();
            this.Visible = false;
            sa.ShowDialog();
        
        }
        string ass,sa;

        private void ara_Load(object sender, EventArgs e)
        {
            conn.Open();
            string shw1 = "select * from User_tbl";
            SqlCommand cmd1 = new SqlCommand(shw1, conn);
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataTable dt1 = new DataTable();
            da1.Fill(dt1);
            dataGridView4.DataSource = dt1;
            dataGridView1.RowHeadersVisible = false;
            dataGridView4.RowHeadersVisible = false;
            conn.Close();
            dataGridView4.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView4.ReadOnly = true;
            dataGridView4.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.ControlDarkDark;
            dataGridView4.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
            dataGridView4.EnableHeadersVisualStyles = false;
            dataGridView4.Columns[0].Visible = false;
            dataGridView4.Columns[3].Visible = false;
            dataGridView4.Columns[1].HeaderText = "Kulanıcı Adı";
            dataGridView4.Columns[1].Width = 125;
            dataGridView4.Columns[2].HeaderText = "Şifre";
            dataGridView4.Columns[4].HeaderText = "Ad";
            dataGridView4.Columns[5].HeaderText = "Soyad";
            dataGridView4.Columns[6].HeaderText = "T.C. Kimlik";
            dataGridView4.Columns[6].Width = 100;
            dataGridView4.Columns[7].HeaderText = "Mail";
            dataGridView4.Columns[7].Width = 275;

            dataGridView4.Refresh();
            conn.Open();
            string shw2 = "select * from Book_tbl";
            SqlCommand cmd2 = new SqlCommand(shw2, conn);
            SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
            DataTable dt2 = new DataTable();
            da2.Fill(dt2);
            dataGridView1.DataSource = dt2;
            conn.Close();
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.ReadOnly = true;
            dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.ControlDarkDark;
            dataGridView1.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
            dataGridView1.EnableHeadersVisualStyles = false;
            dataGridView1.Columns[0].Visible = false;
            dataGridView1.Refresh();
            dataGridView1.Columns[1].HeaderText = "Adı";
            dataGridView1.Columns[2].HeaderText = "Tipi";
            dataGridView1.Columns[3].HeaderText = "Yazarı";
            dataGridView1.Columns[4].HeaderText = "Sayfa";
            dataGridView1.Columns[5].HeaderText = "Yayımcı";
            dataGridView1.Columns[6].HeaderText = "Özeti";
            dataGridView1.Columns[7].HeaderText = "Yılı";

        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {
            

                conn.Open();
                string shw1 = "select * from User_tbl where UID like '%'+@kitapadi+'%'";
                SqlCommand cmd1 = new SqlCommand(shw1, conn);
                cmd1.Parameters.AddWithValue("@kitapadi", textBox5.Text);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                dataGridView4.DataSource = dt1;
                conn.Close();
            dataGridView4.Enabled = true;
            if (dataGridView1.Enabled == true && dataGridView4.Enabled == true)
            {
                MessageBox.Show("Lütfen Kitap Ve Kullanıcıyı Seçiniz", "!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {
       
           
                conn.Open();
                string shw1 = "select * from Book_tbl where QR like '%'+@kitapadi+'%'";
                SqlCommand cmd1 = new SqlCommand(shw1, conn);
                cmd1.Parameters.AddWithValue("@kitapadi", textBox6.Text);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                dataGridView1.DataSource = dt1;
                conn.Close();
            dataGridView1.Enabled = true;
            if (dataGridView1.Enabled == true && dataGridView4.Enabled == true)
            {
                MessageBox.Show("Lütfen Kitap Ve Kullanıcıyı Seçiniz","!",MessageBoxButtons.OK,MessageBoxIcon.Information);
            }
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

            int secilen = dataGridView1.SelectedCells[0].RowIndex;
            label3.Text = dataGridView1.Rows[secilen].Cells[0].Value.ToString();
            sa = dataGridView4.Rows[secilen].Cells[1].Value.ToString();
            if (ass != "" && sa != "")
            {
                try
                {
                    DialogResult res = MessageBox.Show(ass + " Adlı Kullanıcıya " + sa + " Adlı Kitabı Emanet Vermeyi Onaylıyormusunuz ", "?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (res == DialogResult.Yes)
                    {
                        conn.Open();
                        SqlCommand cmd = new SqlCommand(" select count(*) from Brow_Tbl where UserID='" + label1.Text + "'", conn);
                        int sonuc = (int)cmd.ExecuteScalar();
                        conn.Close();
                        if (sonuc == 0)
                        {
                            conn.Open();
                            string query1 = "insert into Brow_tbl(UserID,BookID)values(@1,@2)";
                            SqlCommand cmd1 = new SqlCommand(query1, conn);
                            cmd1.Parameters.AddWithValue("@1", label1.Text);
                            cmd1.Parameters.AddWithValue("@2", label2.Text);
                            cmd1.ExecuteNonQuery();
                            conn.Close();
                            MessageBox.Show("Emanet Verildi", "Bilgi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            MessageBox.Show(ass + " Adlı Kullanıcı da Zaten Emanet Var Lütfen Emaneti Teslim Alıp Tekrar Deneyiniz", " Hata", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Emanet Verilmedi", "//////////////", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("" + ex);
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void dataGridView4_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

            int secilen = dataGridView4.SelectedCells[0].RowIndex;
            label2.Text = dataGridView4.Rows[secilen].Cells[0].Value.ToString();
            ass = dataGridView4.Rows[secilen].Cells[4].Value.ToString();
            if (ass != "" && sa != "")
            {
                try
                {
                    DialogResult res = MessageBox.Show(ass + " Adlı Kullanıcıya " + sa + " Adlı Kitabı Emanet Vermeyi Onaylıyormusunuz ", "?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (res == DialogResult.Yes)
                    {
                        conn.Open();
                        SqlCommand cmd = new SqlCommand(" select count(*) from Brow_Tbl where UserID='" + label1.Text + "'", conn);
                        int sonuc = (int)cmd.ExecuteScalar();
                        conn.Close();
                        if (sonuc == 0)
                        {
                            conn.Open();
                            string query1 = "insert into Brow_tbl(UserID,BookID)values(@1,@2)";
                            SqlCommand cmd1 = new SqlCommand(query1, conn);
                            cmd1.Parameters.AddWithValue("@1", label1.Text);
                            cmd1.Parameters.AddWithValue("@2", label2.Text);
                            cmd1.ExecuteNonQuery();
                            conn.Close();
                            MessageBox.Show("Emanet Verildi", "Bilgi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            MessageBox.Show(ass + " Adlı Kullanıcı da Zaten Emanet Var Lütfen Emaneti Teslim Alıp Tekrar Deneyiniz", " Hata", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Emanet Verilmedi", "//////////////", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("" + ex);
                }
            }

        }
    }
}
