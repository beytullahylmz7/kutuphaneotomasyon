﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Sql;
using System.Data.SqlClient;

namespace Kutuphane
{
    public partial class Prop : Form
    {
        public Prop()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection(@"Data source=.\;Database=.;Integrated Security=true;");
        // SqlConnection conn = new SqlConnection(@"Data source=.\SQLEXPRESS;Database=.;Integrated Security=true;");
        private void Prop_Load(object sender, EventArgs e)
        {
    
            dataGridView1.RowHeadersVisible = false;
            dataGridView4.RowHeadersVisible = false;
            conn.Open();
            string shw = "select * from Book_tbl";
            SqlCommand cmd = new SqlCommand(shw,conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView4.DataSource = dt;      
            dataGridView4.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView4.ReadOnly = true;
            dataGridView4.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.ControlDarkDark;
            dataGridView4.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
            dataGridView4.EnableHeadersVisualStyles = false;
            dataGridView4.Columns[0].Visible = false;
            dataGridView4.Columns[1].HeaderText = "Adı";
            dataGridView4.Columns[2].HeaderText = "Tipi";
            dataGridView4.Columns[3].HeaderText = "Yazarı";
            dataGridView4.Columns[4].HeaderText = "Sayfa";
            dataGridView4.Columns[5].HeaderText = "Yayımcı";
            dataGridView4.Columns[6].HeaderText = "Özeti";
            dataGridView4.Columns[7].HeaderText = "Yılı";
            dataGridView4.Refresh();
            conn.Close();
            conn.Open();
            string shw1 = "select Prop_tbl.ID,Prop_tbl.BookID,BookName,BookType,BookAuthor,PageNumber,Publisher,BookSummary,YearPrint from Book_tbl inner join Prop_tbl on(Book_tbl.ID=Prop_tbl.BookID)";
            SqlCommand cmd2 = new SqlCommand(shw1, conn);
            SqlDataAdapter da1 = new SqlDataAdapter(cmd2);
            DataTable dt1 = new DataTable();
            da1.Fill(dt1);
            dataGridView1.DataSource = dt1;
            conn.Close();
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.ReadOnly = true;
            dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.ControlDarkDark;
            dataGridView1.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
            dataGridView1.EnableHeadersVisualStyles = false;
            dataGridView1.Columns[0].Visible = false;
            dataGridView1.Columns[1].Visible = false;
            dataGridView1.Columns[2].HeaderText = "Adı";
            dataGridView1.Columns[3].HeaderText = "Tipi";
            dataGridView1.Columns[4].HeaderText = "Yazarı";
            dataGridView1.Columns[5].HeaderText = "Sayfa";
            dataGridView1.Columns[6].HeaderText = "Yayımcı";
            dataGridView1.Columns[7].HeaderText = "Özeti";
            dataGridView1.Columns[8].HeaderText = "Yılı";
            dataGridView1.Refresh();    
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Yetkili sa = new Yetkili();
            sa.Visible = true;
            this.Visible = false;
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int secilen = dataGridView1.SelectedCells[0].RowIndex;
            label12.Text = dataGridView1.Rows[secilen].Cells[0].Value.ToString();
            DialogResult Result = MessageBox.Show("Secili Kitabı Tavsiye Edilen Kitaplardan Çıkarmak İstiyormusunuz ? ", "Bilgi", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Result == DialogResult.Yes)
            {
               
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("Delete From Prop_tbl where ID=@1", conn);
                    cmd.Parameters.AddWithValue("@1", label12.Text);
                    cmd.ExecuteNonQuery();
                    conn.Close();
                                
                    
                }
                catch (Exception ex) {
                    MessageBox.Show("Sa" + ex.Message);                
                }
              DialogResult sa =  MessageBox.Show("Kitap Başarıyla Çıkarıldı", "Bilgi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                if (sa == DialogResult.OK)
                {

                    string shw5 = "select * from Prop_tbl inner join Book_tbl on(Book_tbl.ID=Prop_tbl.BookID)";
                  
                     conn.Open();
                    SqlCommand cmd5 = new SqlCommand(shw5, conn);
                    SqlDataAdapter da5 = new SqlDataAdapter(cmd5);
                    DataTable dt5 = new DataTable();
                    da5.Fill(dt5);
                    dataGridView1.DataSource = dt5;
                    conn.Close();
                }

            }
        }

        private void dataGridView4_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {

                int secilen = dataGridView4.SelectedCells[0].RowIndex;
                label11.Text = dataGridView4.Rows[secilen].Cells[0].Value.ToString();

                DialogResult Result = MessageBox.Show("Secili Kitabı Tavsiye Edilen Kitaplar Arasına Taşımak İsiyormusunuz ? ", "Bilgi", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (Result == DialogResult.Yes)
                {
                    try
                    {

                        conn.Open();
                        SqlCommand cmd = new SqlCommand(" select count(*) from Prop_tbl where BookID='" + label11.Text + "'", conn);
                        int sonuc = (int)cmd.ExecuteScalar();
                        conn.Close();
                        if (sonuc == 0)
                        {
                            conn.Open();
                            string query = "insert into Prop_tbl(BookID)values(@1)";
                            SqlCommand cmd1 = new SqlCommand(query, conn);
                            cmd1.Parameters.AddWithValue("@1", label11.Text);
                            cmd1.ExecuteNonQuery();

                            conn.Close();
                         DialogResult sa=  MessageBox.Show("Kitap Tavsiye Edilen Kitaplar Arasına Eklendi ", "Bilgi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                         if (sa == DialogResult.OK)
                         {
                             string shw1 = "select Prop_tbl.ID,Prop_tbl.BookID,BookName,BookType,BookAuthor,PageNumber,Publisher,BookSummary,YearPrint from Book_tbl inner join Prop_tbl on(Book_tbl.ID=Prop_tbl.BookID)";
                             SqlCommand cmd2 = new SqlCommand(shw1, conn);
                             SqlDataAdapter da1 = new SqlDataAdapter(cmd2);
                             DataTable dt1 = new DataTable();
                             da1.Fill(dt1);
                             dataGridView1.DataSource = dt1;
                             conn.Close();
                             dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                             dataGridView1.ReadOnly = true;
                             dataGridView1.Columns[0].Visible = false;
                             dataGridView1.Columns[1].Visible = false;
                             dataGridView1.Columns[2].HeaderText = "Adı";
                             dataGridView1.Columns[3].HeaderText = "Tipi";
                             dataGridView1.Columns[4].HeaderText = "Yazarı";
                             dataGridView1.Columns[5].HeaderText = "Sayfa";
                             dataGridView1.Columns[6].HeaderText = "Yayımcı";
                             dataGridView1.Columns[7].HeaderText = "Özeti";
                             dataGridView1.Columns[8].HeaderText = "Yılı";
                             dataGridView1.Refresh();     
                         }
                        
                        }

                        else if (sonuc > 0)
                        {
                            MessageBox.Show(" Bu Kitap Zaten Tavsiye Edilen Kitaplar Arasında Var  ", "Hata", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    catch(Exception ex)
                    {
                        MessageBox.Show("Kitap Tavsiye Edilen Kitaplar Arasına eklenemedi "+ex.Message, "Hata", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }

              
            }
            catch (Exception ex)
            {
                MessageBox.Show("" + ex);
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
        private Point mouse_offset;
        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                Point mousePos = Control.MousePosition;
                mousePos.Offset(mouse_offset.X, mouse_offset.Y);
                this.Location = mousePos;
            }
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            {
                 mouse_offset = new Point(-e.X, -e.Y);
                        
            }

        }
        private void araisim()
        {
          
            conn.Open();
            string shw1 = "select * from Book_tbl where BookAuthor like '%'+@kitapadi+'%'";
            SqlCommand cmd1 = new SqlCommand(shw1, conn);
            cmd1.Parameters.AddWithValue("@kitapadi", textBox2.Text);
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataTable dt1 = new DataTable();
            da1.Fill(dt1);
            dataGridView4.DataSource = dt1;
            conn.Close();
        }
        private void araisim1()
        {
           
            conn.Open();
            string shw1 = "select * from Book_tbl where BookAuthor like '%'+@kitapadi+'%'";
            SqlCommand cmd1 = new SqlCommand(shw1, conn);
            cmd1.Parameters.AddWithValue("@kitapadi", textBox2.Text);
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataTable dt1 = new DataTable();
            da1.Fill(dt1);
            dataGridView4.DataSource = dt1;
            conn.Close();
        }
        private void araisim2()
        {
      
            conn.Open();
            string shw1 = "select * from Book_tbl where BookAuthor like '%'+@kitapadi+'%'";
            SqlCommand cmd1 = new SqlCommand(shw1, conn);
            cmd1.Parameters.AddWithValue("@kitapadi", textBox2.Text);
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataTable dt1 = new DataTable();
            da1.Fill(dt1);
            dataGridView4.DataSource = dt1;
            conn.Close();

        }
        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            araisim2();

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            araisim1();
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            araisim();
        }

        private void pictureBox3_MouseDown(object sender, MouseEventArgs e)
        {
            mouse_offset = new Point(-e.X, -e.Y);
        }

        private void pictureBox3_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                Point mousePos = Control.MousePosition;
                mousePos.Offset(mouse_offset.X, mouse_offset.Y);
                this.Location = mousePos;
            }
        }
    }
}
