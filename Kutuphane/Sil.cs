﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Sql;
using System.Data.SqlClient;
namespace Kutuphane
{
    public partial class Sil : Form
    {
        public Sil()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection(@"Data source=.\;Database=.;Integrated Security=true;");
        // SqlConnection conn = new SqlConnection(@"Data source=.\SQLEXPRESS;Database=.;Integrated Security=true;");
        private void button6_Click(object sender, EventArgs e)
        {

        }

        private void Sil_Load(object sender, EventArgs e)
        {
            conn.Open();
            string shw = "select * from Book_tbl";
            SqlCommand cmd = new SqlCommand(shw, conn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
            dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.ControlDarkDark;
            dataGridView1.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
            dataGridView1.EnableHeadersVisualStyles = false;
            dataGridView1.RowHeadersVisible = false;
            dataGridView4.RowHeadersVisible = false;
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.ReadOnly = true;
            dataGridView1.Columns[0].Visible = false;
            dataGridView1.Columns[1].HeaderText = "Adı";
            dataGridView1.Columns[2].HeaderText = "Tipi";
            dataGridView1.Columns[3].HeaderText = "Yazarı";
            dataGridView1.Columns[4].HeaderText = "Sayfa";
            dataGridView1.Columns[5].HeaderText = "Yayımcı";
            dataGridView1.Columns[6].HeaderText = "Özeti";
            dataGridView1.Columns[7].HeaderText = "Yılı";
            dataGridView1.Refresh();
            conn.Close();
          
            
            conn.Open();                 
            string shw1 = "select * from User_tbl";
            SqlCommand cmd1 = new SqlCommand(shw1, conn);
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataTable dt1 = new DataTable();
            da1.Fill(dt1);
            dataGridView4.DataSource = dt1;
            conn.Close();
            dataGridView4.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView4.ReadOnly = true;
            dataGridView4.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.ControlDarkDark;
            dataGridView4.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
            dataGridView4.EnableHeadersVisualStyles = false;
            dataGridView4.Columns[0].Visible = false;
            dataGridView4.Columns[3].Visible = false;
            dataGridView4.Columns[1].HeaderText = "Kulanıcı Adı";
            dataGridView4.Columns[2].HeaderText = "Şifre";
            dataGridView4.Columns[4].HeaderText = "Ad";
            dataGridView4.Columns[5].HeaderText = "Soyad";
            dataGridView4.Columns[6].HeaderText = "T.C. Kimlik";
            dataGridView4.Columns[7].HeaderText = "Mail";
            dataGridView4.Columns[7].Width = 190;
            dataGridView4.Refresh();
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Yetkili sa = new Yetkili();
            sa.Visible = true;
            this.Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Yetkili sa = new Yetkili();
            sa.Visible = true;
            this.Hide();
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void dataGridView4_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView4_Click(object sender, DataGridViewCellEventArgs e)
        {
           
        }
        private Point mouse_offset;
        private void groupBox5_Enter(object sender, EventArgs e)
        {

        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                Point mousePos = Control.MousePosition;
                mousePos.Offset(mouse_offset.X, mouse_offset.Y);
                this.Location = mousePos;
            }
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            mouse_offset = new Point(-e.X, -e.Y);

        }

        private void dataGridView4_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int secilen = dataGridView4.SelectedCells[0].RowIndex;
            label13.Text = dataGridView4.Rows[secilen].Cells[0].Value.ToString();
            DialogResult Result = MessageBox.Show("Seçilen Kayıt Silinecektir Onaylıyormsunuz ?", "Dikkat", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Result == DialogResult.Yes)
            {
                try
                {

                    conn.Open();
                    SqlCommand cmd = new SqlCommand("Delete From User_tbl where ID=@1", conn);
                    cmd.Parameters.AddWithValue("@1", label13.Text);
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    MessageBox.Show("Kayıt Başarıyla Silindi", "Bilgi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    string shw = "select * from User_tbl";
                    conn.Open();
                    SqlCommand cmd2 = new SqlCommand(shw, conn);
                    SqlDataAdapter da = new SqlDataAdapter(cmd2);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    dataGridView4.DataSource = dt;
                    conn.Close();
                    dataGridView4.Refresh();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Kayıt Silinemedi" + ex, "Hata", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
        }

        private void dataGridView1_CellDoubleClick_1(object sender, DataGridViewCellEventArgs e)
        {
            int secilen = dataGridView1.SelectedCells[0].RowIndex;
            label10.Text = dataGridView1.Rows[secilen].Cells[0].Value.ToString();
            DialogResult Result = MessageBox.Show("Seçili Kitap Silinecektir Onaylıyormsunuz ?", "Dikkat", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Result == DialogResult.Yes)
            {
                try
                {

                    conn.Open();
                    SqlCommand cmd1 = new SqlCommand("Delete From Prop_tbl where BookID=@1", conn);
                    cmd1.Parameters.AddWithValue("@1", label10.Text);
                    cmd1.ExecuteNonQuery();
                    SqlCommand cmd = new SqlCommand("Delete From Book_tbl where ID=@1", conn);
                    cmd.Parameters.AddWithValue("@1", label10.Text);
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    MessageBox.Show("Kitap Başarıyla Silindi", "Bilgi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    string shw = "select * from Book_tbl";
                    conn.Open();
                    SqlCommand cmd2 = new SqlCommand(shw, conn);
                    SqlDataAdapter da = new SqlDataAdapter(cmd2);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    dataGridView1.DataSource = dt;
                    conn.Close();
                    dataGridView1.Refresh();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Kitap Silinemedi" + ex, "Hata", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }

        }
        private void araisim() {
            conn.Open();
            string shw = "select * from Book_tbl where BookName like '%'+@kitapadi+'%'";
            SqlCommand cmd = new SqlCommand(shw, conn);
            cmd.Parameters.AddWithValue("@kitapadi",textBox1.Text);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
            dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.ControlDarkDark;
            dataGridView1.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
            dataGridView1.EnableHeadersVisualStyles = false;
            dataGridView1.RowHeadersVisible = false;
            dataGridView4.RowHeadersVisible = false;
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.ReadOnly = true;
            dataGridView1.Columns[0].Visible = false;
            dataGridView1.Columns[1].HeaderText = "Adı";
            dataGridView1.Columns[2].HeaderText = "Tipi";
            dataGridView1.Columns[3].HeaderText = "Yazarı";
            dataGridView1.Columns[4].HeaderText = "Sayfa";
            dataGridView1.Columns[5].HeaderText = "Yayımcı";
            dataGridView1.Columns[6].HeaderText = "Özeti";
            dataGridView1.Columns[7].HeaderText = "Yılı";
            dataGridView1.Refresh();
            conn.Close();
        }
        private void araisim1()
        {
            conn.Open();
            string shw = "select * from Book_tbl where BookAuthor like '%'+@kitapadi+'%'";
            SqlCommand cmd = new SqlCommand(shw, conn);
            cmd.Parameters.AddWithValue("@kitapadi", textBox2.Text);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
            conn.Close();
        }
         private void araisim2()
        {
            conn.Open();
            string shw = "select * from Book_tbl where BookType like '%'+@kitapadi+'%'";
            SqlCommand cmd = new SqlCommand(shw, conn);
            cmd.Parameters.AddWithValue("@kitapadi", textBox3.Text);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
            conn.Close();
        }
         private void araisim3()
         {
            
             conn.Open();
             string shw = "select * from User_tbl where UserName like '%'+@kitapadi+'%'";
             SqlCommand cmd = new SqlCommand(shw, conn);
             cmd.Parameters.AddWithValue("@kitapadi", textBox4.Text);
             SqlDataAdapter da = new SqlDataAdapter(cmd);
             DataTable dt = new DataTable();
             da.Fill(dt);
             dataGridView4.DataSource = dt;
             conn.Close();

         }
         private void araisim4()
         {
             conn.Open();
             string shw = "select * from User_tbl where Mail like '%'+@kitapadi+'%'";
             SqlCommand cmd = new SqlCommand(shw, conn);
             cmd.Parameters.AddWithValue("@kitapadi", textBox5.Text);
             SqlDataAdapter da = new SqlDataAdapter(cmd);
             DataTable dt = new DataTable();
             da.Fill(dt);
             dataGridView4.DataSource = dt;
             conn.Close();
         }
    
    
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            araisim();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            araisim1();
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            araisim2();
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            araisim3();
        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {
            araisim4();
        }
    }
}
