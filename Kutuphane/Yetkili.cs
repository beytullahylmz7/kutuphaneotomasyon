﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.Sql;

namespace Kutuphane
{
    public partial class Yetkili : Form
    {
        public Yetkili()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection(@"Data source=.\;Database=.;Integrated Security=true;");
        // SqlConnection conn = new SqlConnection(@"Data source=.\SQLEXPRESS;Database=.;Integrated Security=true;");
        int kitapSayisi = -1
            , kayitSayisi = -1
            , prop=-1
            , emanet=-1;

            
        private void button1_Click(object sender, EventArgs e)
        {
            label1.Visible = true;
            label2.Visible = true;
            label5.Visible = true;
            label17.Visible = true;
            pictureBox2.Visible = true;
            pictureBox1.Visible = true;
            pictureBox4.Visible = true;
           pictureBox5.Visible = true;
            groupBox1.Visible = false;
            try
            {

                conn.Open();
                SqlCommand cmd = new SqlCommand("select count(*) from Book_tbl", conn);
                kitapSayisi = Convert.ToInt32(cmd.ExecuteScalar());
                conn.Close();
                label2.Text = "Toplam Kitap Sayısı : " + kitapSayisi.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show("" + ex);
            }
            try
            {

                conn.Open();
                SqlCommand cmd = new SqlCommand("select count(*) from Brow_Tbl", conn);
                emanet = Convert.ToInt32(cmd.ExecuteScalar());
                conn.Close();
                label17.Text = "Emanette Olan Kitap Sayısı : " + emanet.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show("" + ex);
            }
            try
            {

                conn.Open();
                SqlCommand cmd = new SqlCommand("select count(*) from User_tbl", conn);
                kayitSayisi = Convert.ToInt32(cmd.ExecuteScalar());
                conn.Close();
                label1.Text = "Toplam Kullanıcı Sayısı : " + kayitSayisi.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show("" + ex);
            }
            try
            {

                conn.Open();
                SqlCommand cmd = new SqlCommand("select count(*) from Prop_tbl", conn);
                prop = Convert.ToInt32(cmd.ExecuteScalar());
                conn.Close();
                label5.Text = "Önerilen Kitap Sayısı : " + prop.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show("" + ex);
            }


        }

        private void Yetkili_Load(object sender, EventArgs e)
        {
            textBox6.ReadOnly=true;
            comboBox1.Text = "Seçiniz";
            groupBox1.Visible = false;
            pictureBox5.Visible = true;
            label17.Visible = true;
            try
            {

                conn.Open();
                SqlCommand cmd = new SqlCommand("select count(*) from Brow_Tbl", conn);
                emanet = Convert.ToInt32(cmd.ExecuteScalar());
                conn.Close();
                label17.Text = "Emanette Olan Kitap Sayısı : " + emanet.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show("" + ex);
            }
            try
            {
                Random rastgele = new Random();
                string harfler = "ABCDEFGHIJKLMNOPRSTUVYZXW123456789";
                string uret = "";
                for (int i = 0; i < 6; i++)
                {
                    uret += harfler[rastgele.Next(harfler.Length)];
                }
                textBox6.Text = uret.ToString();
            }
            catch(Exception ex)
            {
                MessageBox.Show(""+ex);
            }
            try
            {

                conn.Open();
                SqlCommand cmd = new SqlCommand("select count(*) from Prop_tbl", conn);
                prop = Convert.ToInt32(cmd.ExecuteScalar());
                conn.Close();
                label5.Text = "Önerilen Kitap Sayısı : " + prop.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show("" + ex);
            }

            try
            {

              
                conn.Open();
                SqlCommand cmd = new SqlCommand("select count(*) from Book_tbl", conn);
                kayitSayisi = Convert.ToInt32(cmd.ExecuteScalar());
                conn.Close();
                label2.Text = "Toplam Kitap Sayısı : " + kayitSayisi.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show("" + ex);
            }
            try
            {
            
                conn.Open();
                SqlCommand cmd = new SqlCommand("select count(*) from User_tbl", conn);
                kayitSayisi = Convert.ToInt32(cmd.ExecuteScalar());
                conn.Close();
                label1.Text = "Toplam Kullanıcı Sayısı : " + kayitSayisi.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show("" + ex);
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            label1.Visible = false;
            label2.Visible = false;
            label5.Visible = false;
            label17.Visible = false;
            pictureBox4.Visible = false;
            pictureBox1.Visible = false;
            pictureBox2.Visible = false;
            pictureBox5.Visible = false;
            groupBox1.Visible=true;

        }

        private void button6_Click(object sender, EventArgs e)
        {
             try
            {
                if (comboBox1.SelectedIndex != 0)
                {
                    conn.Open();
                    string query = "insert into Book_tbl(BookName,BookAuthor,PageNumber,Publisher,BookSummary,YearPrint,BookType,QR)values(@1,@2,@3,@4,@5,@6,@7,@8)";
                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.Parameters.AddWithValue("@1", textBox1.Text);
                    cmd.Parameters.AddWithValue("@2", textBox2.Text);
                    cmd.Parameters.AddWithValue("@3", textBox4.Text);
                    cmd.Parameters.AddWithValue("@4", textBox5.Text);
                    cmd.Parameters.AddWithValue("@5", richTextBox1.Text);
                    cmd.Parameters.AddWithValue("@6", textBox3.Text);
                    cmd.Parameters.AddWithValue("@7", comboBox1.Text);
                    cmd.Parameters.AddWithValue("@8", textBox6.Text);
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    MessageBox.Show("Kitap Başarıyla Eklendi", "Bilgi", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else 
                {
                    MessageBox.Show("Lütfen Kitap Türünü Seçiniz !"," Hata",MessageBoxButtons.OK,MessageBoxIcon.Error);
                }
            }
            catch(Exception ex)
            {

                MessageBox.Show("Lütfen Bilgileri Kontrol Ediniz"+ex, "Hata", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
           

        }

        private void button4_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button3_Click(object sender, EventArgs e)
        {
          Sil f2 = new Sil();
          f2.MdiParent = this.ParentForm; 
          f2.ShowDialog();
          this.Visible = false;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Prop sa = new Prop();
            sa.MdiParent = this.ParentForm; 
            sa.ShowDialog();
            this.Visible = false;
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
        private Point mouse_offset;
        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            mouse_offset = new Point(-e.X, -e.Y);
        
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                Point mousePos = Control.MousePosition;
                mousePos.Offset(mouse_offset.X, mouse_offset.Y);
                this.Location = mousePos;
            }
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsLetter(e.KeyChar) && !char.IsControl(e.KeyChar)
                && !char.IsSeparator(e.KeyChar);
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsLetter(e.KeyChar) && !char.IsControl(e.KeyChar)
                && !char.IsSeparator(e.KeyChar);
        }

        private void textBox5_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsLetter(e.KeyChar) && !char.IsControl(e.KeyChar)
                && !char.IsSeparator(e.KeyChar);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Deposit sa = new Deposit();
            sa.MdiParent = this.ParentForm;
            sa.ShowDialog();
            this.Visible = false;
        }

        private void richTextBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsLetter(e.KeyChar) && !char.IsControl(e.KeyChar)
                && !char.IsSeparator(e.KeyChar);
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }
       
    }      
  
}
   
