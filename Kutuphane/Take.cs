﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Sql;
using System.Data.SqlClient;

namespace Kutuphane
{
    public partial class Take : Form
    {
        public Take()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection(@"Data source=.\;Database=.;Integrated Security=true;");
        // SqlConnection conn = new SqlConnection(@"Data source=.\SQLEXPRESS;Database=.;Integrated Security=true;");

        private void Take_Load(object sender, EventArgs e)
        {
            try
            {
                conn.Open();
                string shw1 = "select Brow_Tbl.ID,Brow_Tbl.BookID,BookName,BookType,BookAuthor,Publisher from Book_tbl inner join Brow_Tbl on(Book_tbl.ID=Brow_Tbl.BookID)";
                SqlCommand cmd2 = new SqlCommand(shw1, conn);
                SqlDataAdapter da1 = new SqlDataAdapter(cmd2);
                DataTable dt1 = new DataTable();
                da1.Fill(dt1);
                dataGridView1.DataSource = dt1;
                conn.Close();
                dataGridView1.RowHeadersVisible = false;
                dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                dataGridView1.ReadOnly = true;
                dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.ControlDarkDark;
                dataGridView1.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
                dataGridView1.EnableHeadersVisualStyles = false;
                dataGridView1.Columns[0].Visible = false;
                dataGridView1.Columns[1].Visible = false;
                dataGridView1.Columns[2].HeaderText = "Adı";
                dataGridView1.Columns[3].HeaderText = "Tipi";
                dataGridView1.Columns[4].HeaderText = "Yazarı";
                dataGridView1.Columns[5].HeaderText = "Yayın";

                dataGridView1.Refresh();
                conn.Open();
                string shw3 = "select Brow_Tbl.ID,Brow_Tbl.UserID,Name,LastName,TC,Mail from User_tbl inner join Brow_Tbl on(User_tbl.ID=Brow_Tbl.UserID)";
                SqlCommand cmd3 = new SqlCommand(shw3, conn);
                SqlDataAdapter da3 = new SqlDataAdapter(cmd3);
                DataTable dt3 = new DataTable();
                da3.Fill(dt3);
                dataGridView2.DataSource = dt3;
                conn.Close();
                dataGridView2.RowHeadersVisible = false;
                dataGridView2.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                dataGridView2.ReadOnly = true;
                dataGridView2.ColumnHeadersDefaultCellStyle.BackColor = SystemColors.ControlDarkDark;
                dataGridView2.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
                dataGridView2.EnableHeadersVisualStyles = false;
                dataGridView2.Columns[0].Visible = false;
                dataGridView2.Columns[1].Visible = false;
                dataGridView2.Columns[2].HeaderText = "Adı";
                dataGridView2.Columns[3].HeaderText = "Soyadı";
                dataGridView2.Columns[4].HeaderText = "T.c.";
               dataGridView2.Columns[5].HeaderText = "Mail";

                dataGridView1.Refresh();
            }
            catch(Exception ex) { MessageBox.Show(""+ex); }
          
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Deposit sa = new Deposit();
            sa.Visible = true;
            this.Visible = false;
        }

        private void panel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
          
        }

        private void dataGridView1_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
        {
            e.Column.SortMode = DataGridViewColumnSortMode.NotSortable;
        }

        private void dataGridView2_ControlAdded(object sender, ControlEventArgs e)
        {
        
        }

        private void dataGridView2_ColumnAdded(object sender, DataGridViewColumnEventArgs e)
        {
            e.Column.SortMode = DataGridViewColumnSortMode.NotSortable;
        }

        private void dataGridView2_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int secilen = dataGridView2.SelectedCells[0].RowIndex;
            label1.Text = dataGridView2.Rows[secilen].Cells[0].Value.ToString();
          
            DialogResult result = MessageBox.Show("Bu Kullanıcıdan Emaneti Geri Alamyı Onaylıyormusunuz", "Bilgi", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("Delete From User_tbl where ID=@1", conn);
                cmd.Parameters.AddWithValue("@1", label1.Text);
                cmd.ExecuteNonQuery();
                conn.Close();
                MessageBox.Show("Kitap Emaneti  Geri Alnıdı", "Bilgi", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            conn.Open();
            string shw1 = "select Brow_Tbl.ID,Brow_Tbl.BookID,BookName,BookType,BookAuthor,Publisher from Book_tbl inner join Brow_Tbl on(Book_tbl.ID=Brow_Tbl.BookID)";
            SqlCommand cmd2 = new SqlCommand(shw1, conn);
            SqlDataAdapter da1 = new SqlDataAdapter(cmd2);
            DataTable dt1 = new DataTable();
            da1.Fill(dt1);
            dataGridView1.DataSource = dt1;
            conn.Close();
            dataGridView1.Refresh();
            conn.Open();
            string shw3 = "select Brow_Tbl.ID,Brow_Tbl.UserID,Name,LastName,TC,Mail from User_tbl inner join Brow_Tbl on(User_tbl.ID=Brow_Tbl.UserID)";
            SqlCommand cmd3 = new SqlCommand(shw3, conn);
            SqlDataAdapter da3 = new SqlDataAdapter(cmd3);
            DataTable dt3 = new DataTable();
            da3.Fill(dt3);
            dataGridView2.DataSource = dt3;
            conn.Close();
            dataGridView2.Refresh();

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

            conn.Open();
            string shw1 = "select * from User_tbl where UID like '%'+@kitapadi+'%'";
            SqlCommand cmd1 = new SqlCommand(shw1, conn);
            cmd1.Parameters.AddWithValue("@kitapadi", textBox5.Text);
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataTable dt1 = new DataTable();
            da1.Fill(dt1);
            dataGridView2.DataSource = dt1;
            conn.Close();
            dataGridView2.Enabled = true;
            if (dataGridView1.Enabled == true && dataGridView2.Enabled == true)
            {
                MessageBox.Show("Lütfen Kitap Ve Kullanıcıyı Seçiniz", "!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {
            conn.Open();
            string shw1 = "select * from Book_tbl where QR like '%'+@kitapadi+'%'";
            SqlCommand cmd1 = new SqlCommand(shw1, conn);
            cmd1.Parameters.AddWithValue("@kitapadi", textBox6.Text);
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataTable dt1 = new DataTable();
            da1.Fill(dt1);
            dataGridView1.DataSource = dt1;
            conn.Close();
            dataGridView1.Enabled = true;
            if (dataGridView1.Enabled == true && dataGridView2.Enabled == true)
            {
                MessageBox.Show("Lütfen Kitap Ve Kullanıcıyı Seçiniz", "!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}

